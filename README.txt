
This module allows you to create a Cyrus IMAP mailbox for each Drupal
user on the system.

Once installed, go to admin/settings/cyrus to configure.

When the module is set up, a 'Mailbox' tab will be present on the user
pages which will allow you to configure the users mailbox.

Cyrus configuration
-------------------

If you want the Drupal user passwords to be used for IMAP login, you
will need to configure Cyrus for this.  There are probably several
different ways to do this.  Here's a configuration that works for me
on a Debian GNU/Linux (Lenny) system, with cyrus-imapd-2.2, saslauthd
and libpam-mysql installed.

 - In /etc/imapd.conf:

 # Assuming you want 'drupalusername@example.com' as your email
 # addresses
 loginrealms: example.com
 sasl_pwcheck_method: saslauthd
 # Using this configuration, you'll need to set this to the name of a
 # Drupal user and use this users details in the Cyrus module settings
 admins: admin

 - In /etc/default/saslauthd

 MECHANISMS="pam"

 - In /etc/pam.d/imap

 auth sufficient pam_mysql.so user=drupal_db_user passwd=drupal_db_pass host=drupal_db_host db=drupal_db_name table=users usercolumn=name passwdcolumn=pass crypt=md5
 account sufficient pam_permit.so
